import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import registerServiceWorker from './registerServiceWorker';
import { Router, Route } from 'react-router-dom';
import BedSensorMonitorView from './container/BedSensorMonitorView';
import LoginView from './container/LoginView';
import history from './history';

import { applyMiddleware, compose, createStore } from 'redux';
import { Provider } from 'react-redux'; //connect, 
import rootReducer from './redux/index';
import thunk from 'redux-thunk';

let middleware = [
    // Analytics, // Kam: not using Google Analytics at this moment
    thunk, // Allows action creators to return functions (not just plain objects)
];

const store = createStore(
    rootReducer,
    undefined,
    compose(
        applyMiddleware(...middleware),
        //autoRehydrate(),
    )
);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <div>
                <Route path='/login' component={LoginView} />
                <Route exact path='/' component={BedSensorMonitorView} />
            </div>
            {/* other routes */}
        </Router>
    </Provider>, document.getElementById('root'));
//ReactDOM.render(<LoginView />, document.getElementById('root'));
export default compose(applyMiddleware(thunk))(createStore)(rootReducer);
//registerServiceWorker();
