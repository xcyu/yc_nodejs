import React, { Component } from 'react';
import * as LoginActions from '../redux/login/actions';
import { FormGroup, FormControl, ControlLabel, Button, Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Dialog from 'material-ui/Dialog';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import { SyncLoader } from 'react-spinners';
const Background = require('../images/login_bg.jpg');
const mapStateToProps = state => ({
    login: state.login.login,
});

const mapDispatchToProps = {
    onLogin: LoginActions.onLogin
}
class LoginView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            emailValue: '',
            passwordValue: '',
            windowHeight: window.innerHeight,
            isPwdValid: true,
            error: '',
            pwdError: '',
            isOpenDialog: false,
            loading: false,
        };
    }

    componentWillMount() {
        if (this.props.login) {
            window.location.href = '/';
        }
        window.onresize = () => { this.setState({ windowHeight: window.innerHeight }); };
    }

    handlePasswordChange(e) {
        this.setState({ passwordValue: e.target.value });
    }

    renderAlertDialog = () => {
        const actions = [
            <FlatButton
                label="OK"
                onClick={() => { this.setState({ isOpenDialog: false }); }}
                style={{ fontFamily: 'adobe-fan-heiti-std', backgroundColor: '#54C3BC', color: '#FCFCFC', marginLeft: 20, width: '20%' }}
            />,
        ];
        return (
            <div>
                <MuiThemeProvider>
                    <Dialog
                        actions={actions}
                        modal={false}
                        open={this.state.isOpenDialog}
                        onRequestClose={() => { this.setState({ isOpenDialog: false }); }}
                        actionsContainerStyle={{ backgroundColor: '#FFF7EE' }}
                        bodyStyle={{ backgroundColor: '#FFF7EE' }}
                    >
                        <span style={{ fontFamily: 'adobe-fan-heiti-std' }}>{this.state.error}</span>
                    </Dialog>
                </MuiThemeProvider>

            </div>
        );

    }

    render() {

        let SubmitButton = withRouter(({ history }) => (
            <Button id='onclikSignIn' style={{ fontFamily: 'adobe-fan-heiti-std', width: 280, marginTop: 30, backgroundColor: '#54C2BB', color: '#FCFCFC', boxShadow: `2px 2px 5px rgba(0, 0, 0, 0.4)` }}
                bsStyle="info" ref={input => this.inputElement = input} onClick={() => {
                    if (this.state.passwordValue === '') {
                        this.setState({ loading: false, isPwdValid: false, pwdError: '密碼不能為空！' });
                    } else {
                        this.setState({ isPwdValid: true, loading: true });
                        this.props.onLogin(this.state.passwordValue, (isValidLogin) => {
                            this.setState({ loading: false });
                            if (isValidLogin) {
                                this.setState({ isPwdValid: true });
                                history.push('/');
                            } else {
                                this.setState({ isPwdValid: false, pwdError: '登入失敗，密碼有誤！' });
                            }
                        }, (e) => {
                            this.setState({ loading: false, error: 'Sorry! 暫時連接不到伺服器，可能是網絡連接有問題。', isOpenDialog: true });
                        });
                    }
                }}>登 入</Button>
        ))

        return (
            <div style={{
                display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: '#D8CBBB', minHeight: this.state.windowHeight,
                backgroundImage: 'url(' + Background + ')', backgroundSize: 'cover', backgroundRepeat: 'no-repeat'
            }} >
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

                <div style={{
                    display: 'flex', justifyContent: 'center', alignItems: 'center', width: 450, height: 400, backgroundSize: 'cover',
                    overflow: 'hidden', backgroundColor: '#FFFBF2', borderRadius: 10, boxShadow: '2px 2px 15px #6E6E6E',
                }} >
                    <Form onSubmit={(e) => { document.getElementById('onclikSignIn').click(); e.preventDefault();}} style={{ padding: 0, width: 250, height: 380, justifyContent: 'center', alignItems: 'center', }} horizontal>
                        <FormGroup
                            controlId="formBasicText">
                            <div style={{ paddingTop: 40, width: '100%', textAlign: 'center' }}>
                                <img alt="" style={{ width: 68, height: 75 }} src={require('../images/bedsensor_logo.png')} />
                            </div>
                            <ControlLabel style={{ fontFamily: 'adobe-fan-heiti-std', fontSize: 30, paddingBottom: 30, paddingTop: 10, width: '100%', textAlign: 'center', color: '#6E6A67' }}>
                                床墊感應儀
                                </ControlLabel>
                            <FormControl
                                autoFocus={true}
                                type="Password"
                                value={this.state.passwordValue}
                                placeholder="密碼"
                                onChange={this.handlePasswordChange.bind(this)}
                                style={{ marginBottom: 10, color: '#6E6A67', fontFamily: 'adobe-fan-heiti-std' }}
                            />
                            {!this.state.isPwdValid && <div style={{ width: '100%', textAlign: 'center', color: '#FF0040', fontFamily: 'adobe-fan-heiti-std' }}>
                                {this.state.pwdError}
                            </div>}
                            <SubmitButton />
                            {/*<Button block style={{ width: 280, marginTop: 30, backgroundColor: '#54C1BB', color: '#FCFCFC' }} onClick={this.loginBtn}>登 入</Button>*/}
                            <FormControl.Feedback />
                        </FormGroup>
                    </Form>
                </div>
                {this.renderAlertDialog()}
                {this.state.loading && <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', position: 'absolute', backgroundColor: '#00000080', minHeight: this.state.windowHeight, width: '100%' }}>
                    <SyncLoader
                        color={'#56DDC3'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginView);
