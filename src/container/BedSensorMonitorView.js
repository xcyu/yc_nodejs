import React, { Component } from 'react';
//import Moment from 'moment';
import { tr, tbody, table } from 'react-bootstrap';
import Select from 'react-select';
import Moment from 'moment';
import axios from 'axios';
import 'moment/locale/zh-hk.js';
import Dialog from 'material-ui/Dialog';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import { connect } from 'react-redux';
//import '../../node_modules/react-select/dist/react-select.css';
import 'react-select/dist/react-select.css';

const Background = require('../images/bg.png');

const mapStateToProps = state => ({
    login: state.login.isLogin,
});

const mapDispatchToProps = {
}

const styles = {

    tableColTitle: {
        //fontFamily: 'source-han-sans-traditional',
        fontFamily: "source-han-sans-traditional",
        fontSize: 18,
        color: '#FFF7EE',
        textAlign: 'center',
        border: 'none',

    },
    tableCell: {
        //fontFamily: 'source-han-sans-traditional',
        fontFamily: 'source-han-sans-traditional',
        fontSize: 18,
        color: '#747170',
        textAlign: 'center',
        margin: 'auto',
        verticalAlign: 'middle',

    },
    tableColTitleRoomNum: {
        fontFamily: 'source-han-sans-traditional',
        fontSize: 18,
        color: '#FFF7EE',
        backgroundColor: '#61A945',
        textAlign: 'center',
        margin: 'auto',
        verticalAlign: 'middle'
    },
    tableCellSelect: {
        fontFamily: 'source-han-sans-traditional',
        fontSize: 16,
        color: '#747170',
        textAlign: 'center',
        margin: 'auto',
        verticalAlign: 'middle'
    },

    tableCellWithAlert: {
        fontFamily: 'source-han-sans-traditional',
        fontSize: 18,
        color: '#FFF7EE',
        textAlign: 'center',
        margin: 'auto',
        verticalAlign: 'middle'
    },

    tableRowWithAlert: {
        backgroundColor: '#FF082C'
    },
    tableRow: {
        backgroundColor: '#FFF7EE'
    },
    tableRowOffline: {
        backgroundColor: '#989898'
    },

    resetButton: {
        backgroundColor: '#898887',
        color: '#FFF7EE',
        fontFamily: 'source-han-sans-traditional',
        fontSize: 16,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 2,
        paddingBottom: 2,
    },
    container: {
        flex: 1,
        backgroundColor: '#FFF7EE',
        height: window.innerHeight,
    },
    pointIn: {
        borderRadius: 12.5,
        width: 25,
        height: 25,
        backgroundColor: '#0EAB9F',
        margin: 'auto'
    },
    pointOff: {
        borderRadius: 12.5,
        width: 25,
        height: 25,
        backgroundColor: '#FFDD15',
        margin: 'auto',
    },
    selectedTab: {
        fontSize: 25,
        color: '#76B45C',
        display: 'inline-block',
        fontWeight: 'bold',
        cursor: 'pointer',
    },
    unSelectedTab: {
        fontSize: 25,
        color: '#CBC9C7',
        display: 'inline-block',
        fontWeight: 'bold',
        cursor: 'pointer',

    }
};
/*
const patientsData = [
    // for stresstest.
    // second floor
    { "residentId": "id-201-1", "bedNo": "201-1", "residentName": "Name-201-1" },
    { "residentId": "id-201-2", "bedNo": "201-2", "residentName": "Name-201-2" },
    { "residentId": "id-202-1", "bedNo": "202-1", "residentName": "Name-202-1" },
    { "residentId": "id-202-2", "bedNo": "202-2", "residentName": "Name-202-2" },
    { "residentId": "id-202-3", "bedNo": "202-3", "residentName": "Name-202-3" },
    { "residentId": "id-203-1", "bedNo": "203-1", "residentName": "Name-203-1" },
    { "residentId": "id-203-2", "bedNo": "203-2", "residentName": "Name-203-2" },
    { "residentId": "id-203-3", "bedNo": "203-3", "residentName": "Name-203-3" },
    { "residentId": "id-204-1", "bedNo": "204-1", "residentName": "Name-204-1" },
    { "residentId": "id-204-2", "bedNo": "204-2", "residentName": "Name-204-2" },
    { "residentId": "id-204-3", "bedNo": "204-3", "residentName": "Name-204-3" },
    { "residentId": "id-205-1", "bedNo": "205-1", "residentName": "Name-205-1" },
    { "residentId": "id-205-2", "bedNo": "205-2", "residentName": "Name-205-2" },
    { "residentId": "id-206-1", "bedNo": "206-1", "residentName": "Name-206-1" },
    { "residentId": "id-206-2", "bedNo": "206-2", "residentName": "Name-206-2" },
    { "residentId": "id-206-3", "bedNo": "206-3", "residentName": "Name-206-3" },
    { "residentId": "id-207-1", "bedNo": "207-1", "residentName": "Name-207-1" },
    { "residentId": "id-207-2", "bedNo": "207-2", "residentName": "Name-207-2" },
    { "residentId": "id-207-3", "bedNo": "207-3", "residentName": "Name-207-3" },
    { "residentId": "id-208-1", "bedNo": "208-1", "residentName": "Name-208-1" },
    { "residentId": "id-208-2", "bedNo": "208-2", "residentName": "Name-208-2" },
    { "residentId": "id-208-3", "bedNo": "208-3", "residentName": "Name-208-3" },
    { "residentId": "id-208-4", "bedNo": "208-4", "residentName": "Name-208-4" },
    { "residentId": "id-209-1", "bedNo": "209-1", "residentName": "Name-209-1" },
    { "residentId": "id-209-2", "bedNo": "209-2", "residentName": "Name-209-2" },
    { "residentId": "id-209-3", "bedNo": "209-3", "residentName": "Name-209-3" },
    { "residentId": "id-210-1", "bedNo": "210-1", "residentName": "Name-210-1" },
    //third floor
    { "residentId": "id-301-1", "bedNo": "301-1", "residentName": "Name-301-1" },
    { "residentId": "id-301-2", "bedNo": "301-2", "residentName": "Name-301-2" },
    { "residentId": "id-302-1", "bedNo": "302-1", "residentName": "Name-302-1" },
    { "residentId": "id-302-2", "bedNo": "302-2", "residentName": "Name-302-2" },
    { "residentId": "id-302-3", "bedNo": "302-3", "residentName": "Name-302-3" },
    { "residentId": "id-303-1", "bedNo": "303-1", "residentName": "Name-303-1" },
    { "residentId": "id-303-2", "bedNo": "303-2", "residentName": "Name-303-2" },
    { "residentId": "id-303-3", "bedNo": "303-3", "residentName": "Name-303-3" },
    { "residentId": "id-304-1", "bedNo": "304-1", "residentName": "Name-304-1" },
    { "residentId": "id-304-2", "bedNo": "304-2", "residentName": "Name-304-2" },
    { "residentId": "id-304-3", "bedNo": "304-3", "residentName": "Name-304-3" },
    { "residentId": "id-305-1", "bedNo": "305-1", "residentName": "Name-305-1" },
    { "residentId": "id-305-2", "bedNo": "305-2", "residentName": "Name-305-2" },
    { "residentId": "id-306-1", "bedNo": "306-1", "residentName": "Name-306-1" },
    { "residentId": "id-306-2", "bedNo": "306-2", "residentName": "Name-306-2" },
    { "residentId": "id-306-3", "bedNo": "306-3", "residentName": "Name-306-3" },
    { "residentId": "id-307-1", "bedNo": "307-1", "residentName": "Name-307-1" },
    { "residentId": "id-307-2", "bedNo": "307-2", "residentName": "Name-307-2" },
    { "residentId": "id-307-3", "bedNo": "307-3", "residentName": "Name-307-3" },
    { "residentId": "id-308-1", "bedNo": "308-1", "residentName": "Name-308-1" },
    { "residentId": "id-308-2", "bedNo": "308-2", "residentName": "Name-308-2" },
    { "residentId": "id-308-3", "bedNo": "308-3", "residentName": "Name-308-3" },
    { "residentId": "id-308-4", "bedNo": "308-4", "residentName": "Name-308-4" },
    { "residentId": "id-309-1", "bedNo": "309-1", "residentName": "Name-309-1" },
    { "residentId": "id-309-2", "bedNo": "309-2", "residentName": "Name-309-2" },
    { "residentId": "id-309-3", "bedNo": "309-3", "residentName": "Name-309-3" },
    { "residentId": "id-310-1", "bedNo": "310-1", "residentName": "Name-310-1" },
    //fifth floor
    { "residentId": "id-501-1", "bedNo": "501-1", "residentName": "Name-501-1" },
    { "residentId": "id-501-2", "bedNo": "501-2", "residentName": "Name-501-2" },
    { "residentId": "id-502-1", "bedNo": "502-1", "residentName": "Name-502-1" },
    { "residentId": "id-502-2", "bedNo": "502-2", "residentName": "Name-502-2" },
    { "residentId": "id-502-3", "bedNo": "502-3", "residentName": "Name-502-3" },
    { "residentId": "id-503-1", "bedNo": "503-1", "residentName": "Name-503-1" },
    { "residentId": "id-503-2", "bedNo": "503-2", "residentName": "Name-503-2" },
    { "residentId": "id-503-3", "bedNo": "503-3", "residentName": "Name-503-3" },
    { "residentId": "id-504-1", "bedNo": "504-1", "residentName": "Name-504-1" },
    { "residentId": "id-504-2", "bedNo": "504-2", "residentName": "Name-504-2" },
    { "residentId": "id-504-3", "bedNo": "504-3", "residentName": "Name-504-3" },
    { "residentId": "id-505-1", "bedNo": "505-1", "residentName": "Name-505-1" },
    { "residentId": "id-505-2", "bedNo": "505-2", "residentName": "Name-505-2" },
    { "residentId": "id-506-1", "bedNo": "506-1", "residentName": "Name-506-1" },
    { "residentId": "id-506-2", "bedNo": "506-2", "residentName": "Name-506-2" },
    { "residentId": "id-506-3", "bedNo": "506-3", "residentName": "Name-506-3" },
    { "residentId": "id-507-1", "bedNo": "507-1", "residentName": "Name-507-1" },
    { "residentId": "id-507-2", "bedNo": "507-2", "residentName": "Name-507-2" },
    { "residentId": "id-507-3", "bedNo": "507-3", "residentName": "Name-507-3" },
    { "residentId": "id-508-1", "bedNo": "508-1", "residentName": "Name-508-1" },
    { "residentId": "id-508-2", "bedNo": "508-2", "residentName": "Name-508-2" },
    { "residentId": "id-508-3", "bedNo": "508-3", "residentName": "Name-508-3" },
    { "residentId": "id-508-4", "bedNo": "508-4", "residentName": "Name-508-4" },
    { "residentId": "id-509-1", "bedNo": "509-1", "residentName": "Name-509-1" },
    { "residentId": "id-509-2", "bedNo": "509-2", "residentName": "Name-509-2" },
    { "residentId": "id-509-3", "bedNo": "509-3", "residentName": "Name-509-3" },
    { "residentId": "id-510-1", "bedNo": "510-1", "residentName": "Name-510-1" },


];

const bedSensorData = {
    "201-1": {
        "heartRate": [
            0
        ],
        "heartRateVariance": [
            0
        ],
        "node": "201-1",
        "oc1": [
            1
        ],
        "oc2": [
            1
        ],
        "oc3": [
            0
        ],
        "respirationRate": [
            0
        ],
        "sigStrength": [
            24616
        ],
        "strokeVolume": [
            0
        ],
        "time": [
            "2018-04-04T10:25:59+00:00"
        ],
        "tLeft": "2018-03-16T10:25:59+00:00"
    },
    "202-2": {
        "heartRate": [
            0
        ],
        "heartRateVariance": [
            0
        ],
        "node": "202-2",
        "oc1": [
            0
        ],
        "oc2": [
            0
        ],
        "oc3": [
            0
        ],
        "respirationRate": [
            0
        ],
        "sigStrength": [
            24616
        ],
        "strokeVolume": [
            0
        ],
        "time": [
            "2018-04-04T11:25:59+00:00"
        ],
        "tLeft": "2018-04-04T12:25:59+00:00"
    },
    "203-1": {
        "heartRate": [
            0
        ],
        "heartRateVariance": [
            0
        ],
        "node": "203-1",
        "oc1": [
            0
        ],
        "oc2": [
            0
        ],
        "oc3": [
            0
        ],
        "respirationRate": [
            0
        ],
        "sigStrength": [
            24616
        ],
        "strokeVolume": [
            0
        ],
        "time": [
            "2018-01-20T10:25:59+00:00"
        ],
        "tLeft": "2018-03-15T12:25:59+00:00"
    },
    "302-1": {
        "heartRate": [
            0
        ],
        "heartRateVariance": [
            0
        ],
        "node": "302-1",
        "oc1": [
            0
        ],
        "oc2": [
            0
        ],
        "oc3": [
            0
        ],
        "respirationRate": [
            0
        ],
        "sigStrength": [
            24616
        ],
        "strokeVolume": [
            0
        ],
        "time": [
            "2018-01-20T10:25:59+00:00"
        ],
        "tLeft": "2018-03-16T13:25:59+00:00"
    }
};
*/
let bedSensorURL = 'http://iohub.ml:8003/data/pull';
let patientsURL = 'http://iohub.ml:8001/endpoint/resident';
//production use only:
if (process.env.REACT_APP_RUNTIME === "production") {
    bedSensorURL = '/data/pull';
    patientsURL = '/endpoint/resident';
}
console.log(process.env.REACT_APP_RUNTIME)
//const tLeftDict = {};

class BedSensorMonitorView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuItems: [
                { value: -1, label: '關' },
                { value: 0, label: '即時' },
                { value: 5, label: '5分鐘' },
                { value: 15, label: '15分鐘' },
                { value: 30, label: '30分鐘' },
            ],
            patientsInfo: [],
            timers: [],
            time: [],
            windowHeight: window.innerHeight,
            isOpenDialog: false,
            selectResetId: null,
            selectedTab2: false,
            selectedTab3: false,
            selectedTabAll: true,
            floor: -1,
            alertMsg: '',
            clearable: false,
            onAllAlert: false,
            offAllAlert: false
        };
    }


    fetchPatientInfo = async () => {
        let p = await axios.get(patientsURL);
        let patients = p.data;
       //let patients = patientsData;
        if (!localStorage.getItem('alertSetting')) {
            let alertSettingInit = [];

            for (let i = 0; i < patients.length; i++) {
                let bedNo = patients[i].bedNo.toString();
                alertSettingInit.push({ id: bedNo, value: -1 });
            }
            localStorage.setItem('alertSetting', JSON.stringify(alertSettingInit));
        }

        try {
            let alertSetting = JSON.parse(localStorage.getItem('alertSetting'));
            let tLeftDict = JSON.parse(localStorage.getItem('alertTimer'));
            if (!tLeftDict) {
                tLeftDict = {};
            }

            //fetch data from server
            let r = await axios.get(bedSensorURL);
            let bedSensor = r.data;

            //let bedSensor = bedSensorData;

            let patientsInfo = [];

            for (let i = 0; i < patients.length; i++) {

                let p = patients[i];
                //#check if this patient.status is false if yes break
                if (p.status === false) { continue; }

                let bedNo = p.bedNo;

                if ((bedNo && this.state.floor === -1) || (bedNo && bedNo.substring(0, 1) === this.state.floor.toString())) {
                    let bedNoArr = bedNo.split('-');
                    let lastN = bedNoArr[1];
                    let numStr = bedNoArr[0];
                    let bedSensorK = bedNo;// numStr + '-' + lastN;
                    let leftT = 0;
                    let isBedsensorOffline = false;
                    let name = p.residentName;
                    // alert('bedSensor info:::'+bedSensor[bedSensorK] + '   key:::' + bedSensorK);
                    if (bedSensor && bedSensor[bedSensorK]) {
                        // should check if state is in bed if yes then reset to 0
                        let time = new Date();
                        let leftPerior = '';
                        let isInBed = bedSensor[bedSensorK].oc2;
                        let ts = new Date(bedSensor[bedSensorK].time);
                        leftT = new Date(bedSensor[bedSensorK].tLeft);
                        console.log("bedSensor:::" + JSON.stringify(bedSensor[bedSensorK]));

                        //console.log(leftT)
                        //if timestamp is too large : > 5 min = 300s * 1000 in mils, this means sensor offline 
                        //skip this entry
                        if (time - ts > 300 * 1000) {
                            //continue;
                            isBedsensorOffline = true;
                        }
                        //add to a global for recording left time of each patient
                        //check if we have record
                        //-------------------local leftT check-----------------
                        //if (tLeftDict && p.bedNo in tLeftDict) {
                        //leftT = tLeftDict[p.bedNo]
                        //} else {
                        //tLeftDict[p.bedNo] = time;
                        //leftT = ts;
                        //}
                        //localStorage.setItem('alertTimer', JSON.stringify(tLeftDict));
                        //------------------local leftT check end ------------

                        let alertSelect = null;
                        for (var g = 0; g < alertSetting.length; g++) {
                            let alert = alertSetting[g];
                            if (alert.id === bedNo.toString()) {
                                alertSelect = alert.value;
                                break;
                            }
                        }
                        if ((!alertSelect && alertSelect !== 0) || isBedsensorOffline) {
                            alertSelect = -1;
                            alertSetting.push({ id: bedNo.toString(), value: -1 });
                            localStorage.setItem('alertSetting', null);
                            localStorage.setItem('alertSetting', JSON.stringify(alertSetting));
                        }

                        let isActivateAlert = false;

                        if (isInBed[0] && isInBed[0] === 1) {
                            isInBed = true;
                            //console.log(isInBed);
                            //-------------------local leftT check-----------------
                            //so we update the last known inbed timestamp
                            //leftT = new Date(bedSensor[bedSensorK].time);
                            //tLeftDict[p.bedNo] = leftT;
                            //localStorage.setItem('alertTimer', null);
                            //localStorage.setItem('alertTimer', JSON.stringify(tLeftDict));
                            //------------------local leftT check end ------------
                        } else {
                            isInBed = false;
                            leftT = new Date(leftT);
                            leftPerior = (time.getTime() - leftT.getTime());

                            var difference = new Date(leftPerior);
                            var diff_mm = difference.getMinutes();
                            //var diff_ss = difference.getSeconds();

                            // format leave perior for display
                            Moment.locale('zh-cn')
                            if (leftPerior > 60 * 1000 * 60) {
                                leftPerior = Moment(leftT).format('YYYY年MM月DD日 a hh:mm:ss');
                            } else {
                                leftPerior = Moment(difference).format('mm:ss');
                            }
                            // if need alert (row get red) 
                            if (alertSelect !== -1 && diff_mm >= alertSelect) {
                                isActivateAlert = true;
                            }
                        }

                        let patientsInfoItem = { id: bedNo.toString(), name: name, roomNum: numStr, bedNum: lastN, time: leftPerior, isInBed: isInBed, alertSelect: alertSelect, isActivateAlert: isActivateAlert, isBedsensorOffline: isBedsensorOffline };

                        patientsInfo.push(patientsInfoItem);
                    } else if (!bedSensor[bedSensorK]) {
                        let patientsInfoItem = { id: bedNo.toString(), name: name, roomNum: numStr, bedNum: lastN, time: '', isInBed: null, alertSelect: -1, isActivateAlert: null, isBedsensorOffline: true };
                        patientsInfo.push(patientsInfoItem);
                    }

                }
            }
            await this.setState({ patientsInfo: patientsInfo });

        } catch (e) {
            console.log(e);
        }
    }

    componentWillMount = async () => {
        document.body.style.backgroundAttachment = 'fixed';
        document.body.style.minHeight = this.state.windowHeight;
        document.body.style.backgroundImage = 'url(' + Background + ')';
        document.body.style.backgroundRepeat = 'no-repeat';
        window.onresize = () => { this.setState({ windowHeight: window.innerHeight }); };

    }

    componentDidMount = async () => {
        if (this.props.login) {
            await this.fetchPatientInfo();
            this.intervalFetchData = setInterval(
                async () => await this.fetchPatientInfo(),
                10 * 1000
            );
        } else {
            window.location.href = '/login';
        }
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
        clearInterval(this.intervalFetchData);
    }

    tick() {
        this.fetchPatientInfo()
        let timer = this.state.timers;
        let displayTimer = [];
        for (let i = 0; i < timer.length; i++) {
            let t = timer[i];
            let d = new Date(timer[i].time);
            let n = new Date();
            displayTimer.push({ id: t.id, time: this.timeFormat(n.getTime() - d.getTime()) });
        }

    }

    logChange = async (val) => {

        let alertSetting = JSON.parse(localStorage.getItem('alertSetting'));

        //alert('logChange:::' + JSON.stringify(val));
        if (alertSetting) {
            let value = val.value;
            let id = val.id;

            for (let j = 0; j < alertSetting.length; j++) {
                let alert = alertSetting[j];
                if (alert.id === id) {
                    alertSetting[j] = { id: id, value: value };
                    break;
                }
            }

            localStorage.setItem('alertSetting', null);
            localStorage.setItem('alertSetting', JSON.stringify(alertSetting));
        }

        let ps = this.state.patientsInfo;

        for (let i = 0; i < ps.length; i++) {
            if (ps[i].id === val.id) {
                ps[i].alertSelect = val.value;
                //alert('Select:::' + JSON.stringify(val) + '***ps:::' + JSON.stringify(ps[i]));
                break;
            }
        }
        this.setState({ patientsInfo: ps });

    }

    _onClick = (id) => {
        this.handleOpen(id, '確定要重置離床時間嗎？');
    }

    renderRow = (arr) => {
        //alert(JSON.stringify(arr));
        return arr.map((patient, index) => {
            //select menu for alert time setting
            let menuItems = this.state.menuItems.map(item => item);
            for (let i = 0; i < menuItems.length; i++) {
                let op = menuItems[i];
                op = { ...menuItems[i], id: patient.id };
                menuItems[i] = op;
            }

            let rowContainerStyle = styles.tableRow;
            let rowContentStyle = styles.tableCell;
            if (patient.isBedsensorOffline) {
                rowContainerStyle = styles.tableRowOffline;
                rowContentStyle = styles.tableCellWithAlert;
            } else if (patient.isActivateAlert) {
                rowContainerStyle = styles.tableRowWithAlert;
                rowContentStyle = styles.tableCellWithAlert;
            }

            return (
                <tr key={patient.id} style={rowContainerStyle}>
                    <td style={rowContentStyle}>{patient.roomNum}</td>
                    <td style={rowContentStyle}>{patient.bedNum}</td>
                    <td style={rowContentStyle}>{patient.name}</td>
                    <td style={styles.tableCellSelect}>
                        <Select
                            value={patient.alertSelect}
                            options={menuItems}
                            onChange={this.logChange}
                            clearable={this.state.clearable}
                            disabled={patient.isBedsensorOffline}
                        />
                    </td>
                    <td style={patient.isActivateAlert ? styles.tableCellWithAlert : styles.tableCell}>
                        {(!patient.isBedsensorOffline && patient.isInBed) && <div style={styles.pointIn}></div>}
                    </td>
                    <td style={styles.tableCell}>
                        {(!patient.isBedsensorOffline && !patient.isInBed) && <div style={styles.pointOff}></div>}
                    </td>
                    <td style={rowContentStyle}>{patient.isBedsensorOffline ? "" : patient.time}</td>
                    {/*<td style={styles.tableCell}>
                        <Button style={styles.resetButton} onClick={() => this._onClick(patient.id)}>重置</Button>
                    </td>*/}
                </tr>
            );
        });

    }

    renderPatientsTable = () => {
        return (
            <table className="table table-bordered" responsive='true' style={{ margin: 'auto', backgroundColor: '#FFFBF2', borderRadius: 5, }}>
                <thead style={{ backgroundColor: '#989898' }}>
                    <tr>
                        <td style={styles.tableColTitle}>房號</td>
                        <td style={styles.tableColTitle}>床號</td>
                        <td style={styles.tableColTitle}>院友姓名</td>
                        <td style={styles.tableColTitle}>提示設定</td>
                        <td style={styles.tableColTitle}>在床</td>
                        <td style={styles.tableColTitle}>離床</td>
                        <td style={styles.tableColTitle}>離床時間</td>
                        {/*<td style={styles.tableColTitle}>重置警報</td>*/}
                    </tr>
                </thead>
                <tbody>
                    {this.renderRow(this.state.patientsInfo.sort((a, b) => a.id.split('-').join('') - b.id.split('-').join('')))}
                </tbody>
            </table>

        );
    }

    handleClose = () => {
        this.setState({ isOpenDialog: false, selectResetId: null });
    };

    handleOpen = (id, msg) => {
        this.setState({ isOpenDialog: true, selectResetId: id, alertMsg: msg });
    };

    handleOk = () => {
        let patients = this.state.patientsInfo;
        if (this.state.onAllAlert) {
            let alertSettingInit = [];
            for (let i = 0; i < patients.length; i++) {
                let bedNo = patients[i].id.toString();
                alertSettingInit.push({ id: bedNo, value: 0 });
            }
            // localStorage.setItem('alertTimer', null);
            localStorage.setItem('alertSetting', JSON.stringify(alertSettingInit));
            this.setState({ isOpenDialog: false, onAllAlert: false });

        } else if (this.state.offAllAlert) {
            let alertSettingInit = [];
            for (let i = 0; i < patients.length; i++) {
                let bedNo = patients[i].id.toString();
                alertSettingInit.push({ id: bedNo, value: -1 });
            }
            // localStorage.setItem('alertTimer', null);
            localStorage.setItem('alertSetting', JSON.stringify(alertSettingInit));
            this.setState({ isOpenDialog: false, offAllAlert: false });
        }

        // if (this.state.selectResetId) {
        //     let tLeftDict = JSON.parse(localStorage.getItem('alertTimer'));
        //     let patientsInfo = this.state.patientsInfo;
        //     for (let i = 0; i < patientsInfo.length; i++) {
        //         let p = patientsInfo[i];
        //         if (this.state.selectResetId && p.id === this.state.selectResetId) {
        //             p.isActivateAlert = false;
        //             tLeftDict[p.id] = new Date();
        //             localStorage.setItem('alertTimer', null);
        //             //alert('new tLeftDict::' + JSON.stringify(tLeftDict));
        //             localStorage.setItem('alertTimer', JSON.stringify(tLeftDict));
        //             break;
        //         }
        //     }
        //     this.setState({ isOpenDialog: false, patientsInfo });

        // } else {
        //     let alertSettingInit = [];

        //     for (let i = 0; i < patients.length; i++) {
        //         let bedNo = patients[i].bedNo.toString();
        //         alertSettingInit.push({ id: bedNo, value: -1 });
        //     }
        //     localStorage.setItem('alertTimer', null);
        //     localStorage.setItem('alertSetting', JSON.stringify(alertSettingInit));
        //     this.setState({ isOpenDialog: false });
        // }
    };


    renderAlertDialog = () => {
        const actions = [
            <FlatButton
                label="取消"
                onClick={this.handleClose}
                style={{ backgroundColor: '#989898', color: '#FFF7EE', width: '20%', fontFamily: 'source-han-sans-traditional', }}
            />,
            <FlatButton
                label="確定"
                onClick={this.handleOk}
                style={{ backgroundColor: '#E75125', color: '#FFF7EE', marginLeft: 20, width: '20%', fontFamily: 'source-han-sans-traditional', }}
            />,
        ];

        return (
            <div>
                <MuiThemeProvider>
                    <Dialog
                        actions={actions}
                        modal={false}
                        open={this.state.isOpenDialog}
                        onRequestClose={this.handleClose}
                        actionsContainerStyle={{ backgroundColor: '#FFF7EE' }}
                        bodyStyle={{ backgroundColor: '#FFF7EE' }}
                    >
                        <span style={{ fontFamily: 'source-han-sans-traditional' }}>{this.state.alertMsg}</span>

                    </Dialog>
                </MuiThemeProvider>

            </div>
        );

    }
    handleChangeTab = (e) => {

        if (e.value === 2) {
            this.setState({ floor: 2, selectedTabAll: false, selectedTab2: true, selectedTab3: false });
        } else if (e.value === 3) {
            this.setState({ floor: 3, selectedTabAll: false, selectedTab2: false, selectedTab3: true });
        } else {
            this.setState({ floor: -1, selectedTabAll: true, selectedTab2: false, selectedTab3: false });
        }
    }

    resetAll = () => {

        this.handleOpen(null, '確定要重置全部離床時間嗎？');

    }

    offAllAlert = () => {
        this.setState({ offAllAlert: true });
        this.handleOpen(null, '確定要關閉所有提醒嗎？');
    }

    onAllAlert = () => {
        this.setState({ onAllAlert: true });
        this.handleOpen(null, '確定要開啟所有即時提醒嗎？');
    }

    render() {
        let menuItems = [
            { value: -1, label: '全部院友' },
            { value: 2, label: '二樓院友' },
            { value: 3, label: '三樓院友' },
        ];

        return (
            <div style={{ minHeight: this.state.windowHeight }}>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
                <div style={{ padding: 20, backgroundColor: '#60AB46', textAlign: 'center' }}>
                    <img alt="" style={{ width: 45, height: 50, display: 'inline-block', verticalAlign: 'bottom', marginRight: 15 }} src={require('../images/bedsensor_title.png')} />
                    <div style={{ fontFamily: 'source-han-sans-traditional', fontSize: 30, backgroundColor: '#60AB46', color: '#FFF7EE', textAlign: 'center', display: 'inline-block', }}>
                        床墊感應儀
                    </div>
                </div>
                <div style={{ boxShadow: '0px 5px 15px #6E6E6E', borderBottomLeftRadius: 20, borderBottomRightRadius: 20, backgroundColor: '#FFFBF2', marginLeft: 30, marginRight: 30, minHeight: this.state.windowHeight - 110, position: 'relative' }}>
                    <button style={{
                        border: 0, borderRadius: 5, backgroundColor: '#989898', color: '#FFF7EE', height: 38, width: 150, position: 'absolute',
                        right: 550, top: 25, textAlign: 'center', fontFamily: 'source-han-sans-traditional', fontSize: 20, paddingBottom: 2
                    }}
                        onClick={this.offAllAlert}>關閉所有提醒</button>
                    <button style={{
                        border: 0, borderRadius: 5, backgroundColor: '#989898', color: '#FFF7EE', height: 38, width: 200, position: 'absolute',
                        right: 330, top: 25, textAlign: 'center', fontFamily: 'source-han-sans-traditional', fontSize: 20, paddingBottom: 2
                    }}
                        onClick={this.onAllAlert}>開啟所有即時提醒</button>
                    <div style={{ position: 'absolute', width: 250, right: 60, top: 25, textAlign: 'center', fontFamily: 'source-han-sans-traditional', fontSize: 20, color: '#747170' }}>
                        <Select
                            value={this.state.floor}
                            options={menuItems}
                            onChange={this.handleChangeTab}
                            clearable={this.state.clearable}
                        />
                    </div>

                    <div style={{ backgroundColor: '#FFFBF2', position: 'absolute', marginTop: 80, width: '100%' }}>
                        {this.renderPatientsTable()}
                    </div>

                    {/*<div style={{ padding: 10, backgroundColor: '#FFFBF2', textAlign: 'left', marginLeft: 30, marginRight: 30, }}>
                    <div style={this.state.selectedTabAll ? styles.selectedTab : styles.unSelectedTab} onClick={() => { this.handleChangeTab('all') }}>
                        全部院友
                    </div>
                    <div style={{ width: 25, display: 'inline-block' }} />
                    <div style={this.state.selectedTab2 ? styles.selectedTab : styles.unSelectedTab} onClick={() => { this.handleChangeTab('second') }}>
                        二樓院友
                    </div>
                    <div style={{ width: 25, display: 'inline-block' }} />
                    <div style={this.state.selectedTab3 ? styles.selectedTab : styles.unSelectedTab} onClick={() => { this.handleChangeTab('third') }}>
                        三樓院友
                    </div>
                </div>*/}

                </div>
                {this.renderAlertDialog()}
            </div>


        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(BedSensorMonitorView);
