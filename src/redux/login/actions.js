import axios from 'axios';

const auth_resolve = (isLogin) => {
    return { type: 'LOGIN_STATE', isLogin }
}

export const onLogin = (pwd, resolve, reject) => {
    return async (dispatch, getState) => {

        //  resolve(true);
        //  dispatch(auth_resolve(true));
        ///////////////login api////////////////////
       let loginURL = 'http://iohub.ml:8004/api/login/';
        
        if (process.env.REACT_APP_RUNTIME === "production") {
            loginURL = '/api/login/';
        }
         let isValidResult = await axios.get(loginURL + pwd).catch((error) => {
             console.log(error);
             reject(error);
             return;
         });
         if (isValidResult) {
             let isValid = isValidResult.data.verified;
             dispatch(auth_resolve(isValid));
             resolve(isValid);
         }
        }
}
