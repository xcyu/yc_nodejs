const initialState = { isLogin: false };

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case 'LOGIN_STATE':
            return { ...state, isLogin: action.isLogin };
        default:
            return state;
    }
}